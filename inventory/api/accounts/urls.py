from django.urls import path

from .views import api_user_token, list_accounts, account_detail

urlpatterns = [
    path("accounts/",list_accounts, name="list_accounts"),
    path(
        "accounts/<str:email>/",
        account_detail,
        name="api_account_detail",
    ),
    path("api/tokens/mine/", api_user_token, name="api_user_token")
]