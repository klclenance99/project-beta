import React from 'react';
import { useState, useEffect } from 'react'
import { Line } from 'react-chartjs-2'
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);

function MainPage() {
  let [aList, setAList] = useState({appointments: []})

  async function fetchAppointments() {
    let token = localStorage.getItem('token')
    const res = await fetch('http://localhost:8080/api/appointments/', {
      headers: {Authorization: `Bearer ${token}`}, })
      if (!res.ok) {
        window.location.href = "login"
        alert("you have been signed out please log back in")
        console.log("error")
      }
    const newAList = await res.json()
    setAList(newAList)
  }
  useEffect(() => {fetchAppointments()}, [])
  function appointmentsPerMonth(month) {
    return aList.appointments.filter(appointment => (appointment.date_revised.substring(5, 7) - 1) === month).length
  }

  function yearlyAppts() {
    let i = 0
    let arr = []
    while (i <= 11) {
      arr.push(appointmentsPerMonth(i))
      i++
    }
    return arr
  }

  let [sList, setSList] = useState({sales: []})

  async function fetchSales() {
    let token = localStorage.getItem('token')
    const res = await fetch('http://localhost:8090/api/sales/', {
      headers: {Authorization: `Bearer ${token}`}, })
      if (!res.ok) {
        window.location.href = "login"
        alert("you have been signed out please log back in")
        console.log("error")
      }
    const newSList = await res.json()
    setSList(newSList)
  }
  useEffect(() => {fetchSales()}, [])
  function salesPerMonth(month) {
    return sList.sales.filter(sale => (sale.sold_date.substring(5, 7) - 1) === month).length
  }

  function yearlySales() {
    let i = 0
    let arr = []
    while (i <= 11) {
      arr.push(salesPerMonth(i))
      i++
    }
    return arr
  }

  // const labels = Utils.months({count: 12});
  // label: 'Service Center Performance'
  // data: yearlyAppts()
  // fill: false
  // borderColor: 'rgb(75, 192, 192)'
  // tension: 0.1
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <h2>2022 Service Center Performance</h2>
        <Line 
        data={{
          labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          datasets: [{
            label: "appointments completed for 2022",
            data: yearlyAppts()
          }]
        }}
        height={400}
        width={600}
        />
        <h2>2022 Sales Center Performance</h2>
        <Line 
        data={{
          labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
          datasets: [{
            label: "sales completed for 2022",
            data: yearlySales()
          }]
        }}
        height={400}
        width={600}
        />
      </div>
    </div>
  );
}

export default MainPage;
