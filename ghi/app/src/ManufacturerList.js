import { useState, useEffect } from 'react';

function ManufacturerList() {
  let [mList, setMList] = useState({manufacturers: []})

  async function fetchManufacturers() {
    let token = localStorage.getItem('token')
    const res = await fetch('http://localhost:8100/api/manufacturers/', {
      headers: {Authorization: `Bearer ${token}`}, })
    if (!res.ok) {
      window.location.href = "login"
      alert("you have been signed out please log back in")
      console.log("error")
    }
    const newMList = await res.json()
    setMList(newMList)
  }
  useEffect(() => {fetchManufacturers()}, [])
      return (
          <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
          {mList.manufacturers.map(manufacturer => {
          return (
         <tr key={manufacturer.id}>
           <td>{ manufacturer.name }</td>
         </tr>
            );
             })}
          </tbody>
        </table>
      );
  }
  
  export default ManufacturerList;